var Stages = {
    "Default": {
        "background": "#208020",
        "layers": {
            "pendulum": [
                { "type": "swinger", "x": 0, "y": 2 },
                { "type": "uspsa", "x": 0, "y": -0.5 }
            ],
            "front": [
                { "type": "uspsa-back", "x": -2, "y": 1.5, "rotate": -90 },
                { "type": "uspsa-back", "x": 0.4, "y": 1.5, "rotate": 90 },
                { "type": "uspsa-back", "x": 2, "y": 1.5, "rotate": 90 }
            ]
        }
    },
    "Right": {
        "background": "saddlebrown",
        "woundAt": -90,
        "layers": {
            "pendulum": [
                { "type": "swinger", "x": -1, "y": 1  },
                { "type": "uspsa", "x": -1, "y": -1 }
            ],
            "front": [
                { "type": "uspsa-back", "x": -1, "y": -0.7 },
                { "type": "uspsa-back", "x": -1, "y": 1 },
                { "type": "uspsa-back", "x": -2, "y": -0.7 },
                { "type": "uspsa-back", "x": -2, "y": 1 }
            ]
        }
    },
    "Left (IPSC)": {
        "layers": {
            "pendulum": [
                { "type": "swinger", "x": 1, "y": 1  },
                { "type": "ipsc", "x": 1, "y": -1 }
            ],
            "front": [
                { "type": "ipsc-back", "x": 1, "y": -0.7 },
                { "type": "ipsc-back", "x": 1, "y": 0 },
                { "type": "ipsc-back", "x": 2, "y": -0.7 },
                { "type": "ipsc-back", "x": 3, "y": 0 }
            ]
        }
    }
};
