function createPendilumSimulation(time_ms, pen) {
    let { len, m, friction, alpha } = pen;
    const g = 9.8; // m / s / s
    if (typeof woundAt !== "undefined") {
        alpha = woundAt;
    }
    let vel = 0; // m / s
    const sim = () => {
        var delta_t = 0.001; // s
        var f = m * g; // kg * m / s / s
        var push =
            -f * Math.sin(alpha) - friction * Math.sign(vel);
        var new_vel = vel + push * delta_t / m; // m / s
        var dist = (vel + new_vel) / 2 * delta_t;
        var delta_alpha = dist / len;
        vel = new_vel;
        alpha += delta_alpha;
        ++time_ms;
    };
    return {
        get time() { return time_ms; },
        get alpha() { return alpha; },
        runUntil(new_time_ms) {
            while (time_ms < new_time_ms) {
                sim();
            }
        },
    };
}