var swingerInfo = {
    len: 0.3, // m
    m: 10, // kg
    friction: 0.7, // kg * m / s / s
};

var swingerControl = {
    onRun: null,
    onStop: null,
};

$(function () {
    $("#swinger_dialog").dialog({
        autoOpen: false,
        width: 'auto',
        closeOnEscape: false,
        buttons: [
            {
                text: "Run",
                click: function() {
                    swingerControl.onRun && swingerControl.onRun();
                }
            },
            {
                text: "Stop",
                click: function() {
                    swingerControl.onStop && swingerControl.onStop();
                }
            },
            {
              text: "Close",
              click: function() {
                $( this ).dialog( "close" );
              }
            }
          ],
    });

    function updateFields() {
        $("#swinger_arm_length").val(swingerInfo.len);
        $("#swinger_arm_weight").val(swingerInfo.m);
        $("#swinger_friction").val(swingerInfo.friction);
    }
    updateFields();

    function swingerInfoChanged() {}

    $("#swinger_arm_length").change(function () {
        swingerInfo.len = +$( this ).val();
        swingerInfoChanged();
    });
    $("#swinger_arm_weight").change(function () {
        swingerInfo.m = +$( this ).val();
        swingerInfoChanged();
    });
    $("#swinger_friction").change(function () {
        swingerInfo.friction = +$( this ).val();
        swingerInfoChanged();
    });
});
