const svgNs = "http://www.w3.org/2000/svg";

var ImpConv = {
    IN_2_CM: 2.54,
    CM_2_IN: 1/2.54,
    IN_2_FT: 1/12,
    FT_2_IN: 12,
    IN_2_YD: 1/36,
    YD_2_IN: 36,
    YD_2_M: 100 / 36 / 2.54,
    M_2_YD: 36 * 2.54 / 100,
};

function inchToPt(val) {
    return Math.round((val || 0) / 18 * 300);
}

function cmToPt(val) {
    return inchToPt(val * ImpConv.CM_2_IN);
}

function feetToPt(val) {
    return inchToPt(val * ImpConv.FT_2_IN);
}

function buildObj(obj) {
   const info = getResourceInfo(obj.type);
   const g = document.createElementNS(svgNs, "g");
   g.setAttribute("transform", `translate(${feetToPt(obj.x || 0)} ${feetToPt(obj.y || 0)}) rotate(${obj.rotate || 0})`)
   const img = document.createElementNS(svgNs, "image");
   img.setAttribute("width", info.width);
   img.setAttribute("height", info.height);
   img.setAttribute("x", -info.cx);
   img.setAttribute("y", -info.cy);
   img.setAttributeNS("http://www.w3.org/1999/xlink", "xlink:href", info.path);
   g.appendChild(img);
   return g;
}

function buildLayer(svg, layer) {
    svg.textContent = "";
    if (!layer) return;
    for (let i of layer) {
        svg.appendChild(buildObj(i));
    }
}

function buildStage(stage) {
    const { layers, background } = stage;
    const backgroundSvg = document.getElementById("background");
    backgroundSvg.setAttribute("fill", background || "#208020");
    const frontSvg = document.getElementById("front-layer");
    buildLayer(frontSvg, layers.front);
    const pendulumSvg = document.getElementById("pendulum-layer");
    buildLayer(pendulumSvg, layers.pendulum);
    const backSvg = document.getElementById("back-layer");
    buildLayer(backSvg, layers.back);
}

function getSwingerRotationPoint(stage) {
    const { layers } = stage;
    const swinger = layers.pendulum.find(i => i.type == "swinger");
    return {
        x: feetToPt(swinger.x || 0),
        y: feetToPt(swinger.y || 0),
    };
}

function setSwingerRotation(angle, point) {
    const deg = angle / Math.PI * 180;
    const cx = point.x;
    const cy = point.y;
    document.getElementById("pendulum-layer").setAttribute("transform", 
      `rotate(${deg} ${cx} ${cy})`);
}

function setViewBoxSize(width, height, imperial) {
    const w = imperial ? inchToPt(width) : cmToPt(width);
    const h = imperial ? inchToPt(height) : cmToPt(height);
    document.getElementById("scene").setAttribute("viewBox",
        `${-w / 2} ${-h / 2} ${w} ${h}`);
}
