var screenInfo = {
    imperial: true,
    width: 10.5,
    height: 6.25,
    monitor: 3,
    distance: 21,
    onChanged: null,
    convertTo(imperial) {
        if (this.imperial == imperial) return;
        if (imperial) {
            this.width *= ImpConv.CM_2_IN;
            this.height *= ImpConv.CM_2_IN;
            this.monitor *= ImpConv.M_2_YD;
            this.distance *= ImpConv.M_2_YD;
        } else {
            this.width *= ImpConv.IN_2_CM;
            this.height *= ImpConv.IN_2_CM;
            this.monitor *= ImpConv.YD_2_M;
            this.distance *= ImpConv.YD_2_M;
        }
        this.imperial = imperial;
    },
    load() {
        var loaded = localStorage["sim.screen_info"];
        if (loaded) {
            loaded = loaded.split(",");
            this.imperial = loaded[0] == "i";
            this.width = +loaded[1];
            this.height = +loaded[2];
            this.monitor = +loaded[3];
            this.distance = +loaded[4];
        }
    },
    save() {
        var parts = [
            this.imperial ? "i" : "m",
            this.width, this.height, this.monitor, this.distance
        ];
        localStorage["sim.screen_info"] = parts.join(",");
    }
};

$(function () {
    $("#screen_dialog").dialog({
        autoOpen: false,
        width: 'auto',
        closeOnEscape: false,
        buttons: [
            {
              text: "Close",
              click: function() {
                $( this ).dialog( "close" );
              }
            }
          ],
    });

    screenInfo.load();

    if (screenInfo.imperial) {
        $("#screen_system_imperial").attr('checked', true);
    } else {
        $("#screen_system_metric").attr('checked', true);
    }


    function updateFields() {
        $("#screen_width").val(screenInfo.width);
        $("#screen_height").val(screenInfo.height);
        $("#screen_distance").val(screenInfo.monitor);
        $("#target_distance").val(screenInfo.distance);
    }

    function updateScreenSystem() {
        const is_imperial = $("#screen_system_imperial").is(":checked");
        screenInfo.convertTo(is_imperial);
        updateFields();
        $(".screen_system_value_small").text(is_imperial ? "in" : "cm");
        $(".screen_system_value_large").text(is_imperial ? "yd" : "m");
        screenInfoChanged();
    }

    function screenInfoChanged() {
        screenInfo.save();
        screenInfo.onChanged && screenInfo.onChanged();
    }

    $("#screen_system_imperial").click(updateScreenSystem);
    $("#screen_system_metric").click(updateScreenSystem);

    $("#screen_width").change(function () {
        screenInfo.width = +$( this ).val();
        screenInfoChanged();
    });
    $("#screen_height").change(function () {
        screenInfo.height = +$( this ).val();
        screenInfoChanged();
    });
    $("#screen_distance").change(function () {
        screenInfo.monitor = +$( this ).val();
        screenInfoChanged();
    });
    $("#target_distance").change(function () {
        screenInfo.distance = +$( this ).val();
        screenInfoChanged();
    });

    updateScreenSystem();
});