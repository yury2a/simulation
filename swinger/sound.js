async function initListen() {
    const lowFrequency = 85, highFrequency = 1800;
    const soundDuration = 10;
    const debounceTimeout = 1000;
    const triggerDecibels = -30;

    const mediaDevices = navigator.mediaDevices || {};

    if (mediaDevices.getUserMedia === undefined) {
        mediaDevices.getUserMedia = function (constraints) {
            var getUserMedia = navigator.webkitGetUserMedia || navigator.mozGetUserMedia || navigator.msGetUserMedia;
            if (!getUserMedia) {
                return Promise.reject(
                    new Error('getUserMedia is not implemented in this browser'));
            }
            return new Promise(function (resolve, reject) {
                getUserMedia.call(navigator, constraints, resolve, reject);
            });
        }
    }

    var audioCtx = new (window.AudioContext || window.webkitAudioContext)();

    var analyser = audioCtx.createAnalyser();
    analyser.minDecibels = -90;
    analyser.maxDecibels = -10;
    analyser.smoothingTimeConstant = 0.6;
    analyser.fftSize = 256;

    const freqByteData = new Uint8Array(analyser.frequencyBinCount);
    const fr = audioCtx.sampleRate / analyser.fftSize;
    const lowBin = Math.floor(lowFrequency / fr);
    const highBin = Math.ceil(highFrequency / fr);
    const triggerValue = Math.round((triggerDecibels - analyser.minDecibels) * 256 /
        (analyser.maxDecibels - analyser.minDecibels));

    var constraints = { audio: true };
    const stream = await mediaDevices.getUserMedia(constraints);

    const source = audioCtx.createMediaStreamSource(stream);
    source.connect(analyser);

    let lastSoundTime = null;
    let debounceSourceUntil = 0;
    const checkFrequencyData = () => {
        analyser.getByteFrequencyData(freqByteData);

        let sound = false;
        for (let i = lowBin; i <= highBin; i++) {
            if (freqByteData[i] > triggerValue) {
                sound = true;
            }
        }
        if (sound && debounceSourceUntil <= Date.now()) {
            if (lastSoundTime != null) {
                lastSoundTime = Date.now();
            } else if (lastSoundTime + soundDuration <= Date.now()) {
                res.onSound && (0, res.onSound)();
                debounceSourceUntil = Date.now() + debounceTimeout;
            }
        } else {
            lastSoundTime = null;
        }
    }

    const int_id = setInterval(checkFrequencyData, 1);
    const res = {
        onSound: null,
        stop() {
            clearInterval(int_id);
            source.disconnect();
            if (stream.stop) {
                stream.stop(); // old way
            } else {
                for (let track of stream.getTracks()) track.stop();
            }
            return audioCtx.close();
        }
    };
    return res;
}