var sim = null;
var rafId;

var app = {
    stage: "Default",
    listen: false,
    "screen..."() {
        $('#screen_dialog').dialog('open');
    },
    "swinger..."() {
        $('#swinger_dialog').dialog('open');
    },
    run() {
        var pen = {
            len: swingerInfo.len,
            m: swingerInfo.m,
            friction: swingerInfo.friction,
            alpha: Math.PI / 2,
        };
        if (typeof currentStage.woundAt !== "undefined") {
            pen.alpha = currentStage.woundAt / 180 * Math.PI;
        }
        
        sim = createPendilumSimulation(Date.now(), pen);
        rafId = requestAnimationFrame(step);
    },
    stop() {
        document.getElementById("pendulum-layer").setAttribute("transform", "");
        cancelAnimationFrame(rafId);
        sim = null;
    },
    fullScreen() {
        document.body.requestFullscreen();
    }
};

var gui = new dat.GUI();
gui.add(app, "stage", Object.keys(Stages)).onFinishChange(updatePreset);
gui.add(app, 'swinger...');
gui.add(app, 'screen...');
gui.add(app, 'run');
gui.add(app, 'stop');
gui.add(app, 'fullScreen');
gui.add(app, 'listen').onFinishChange(updateSoundListen);

screenInfo.onChanged = updateViewBox;
swingerControl.onRun = app.run;
swingerControl.onStop = app.stop;

var sim_start;
function step() {
    if (!sim) return;
    sim.runUntil(Date.now());
    update();
    rafId = requestAnimationFrame(step);
}

function update() {
    const alpha = sim.alpha;
    setSwingerRotation(alpha, pendulumLayerCenter);
}

function updateViewBox() {
    const scale = screenInfo.monitor / screenInfo.distance;
    setViewBoxSize(
        screenInfo.width / scale,
        screenInfo.height / scale,
        screenInfo.imperial
    );
}
updateViewBox();

let pendulumLayerCenter;
let currentStage;
function updatePreset() {
    currentStage = Stages[app.stage];
    pendulumLayerCenter = getSwingerRotationPoint(currentStage)
    buildStage(currentStage);
}
updatePreset();

function onSound() {
    app.run();
}

var soundListenObj = null;
function updateSoundListen() {
    if (app.listen) {
        initListen().then(obj => {
            soundListenObj = obj;
            obj.onSound = onSound;
        }, () => {
            sound.listen = false;
        })
    } else {
        soundListenObj.stop();
    }
}

document.addEventListener("keydown", function(e) {
    switch (e.key) {
        case " ":
            app.run();
            e.preventDefault();
            break;
        case "Escape":
            app.stop();
            e.preventDefault();
            break;
    }
});
