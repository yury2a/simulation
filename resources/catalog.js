const resourceInfo = {
    "uspsa": { width: 300, height: 500, cx: 150, cy: 200 },
    "uspsa-back": { width: 300, height: 500, cx: 150, cy: 200 },
    "ipsc": { width: 300, height: 480, cx: 150, cy: 120 },
    "ipsc-back": { width: 300, height: 480, cx: 150, cy: 120 },
    "swinger": { width: 300, height: 300, cx: 150, cy: 240 },
};

function getResourceInfo(name) {
   const resource = resourceInfo[name];
   if (!resource) throw new Error(`resource ${name} is not found`);
   return {
       width: resource.width,
       height: resource.height,
       cx: resource.cx,
       cy: resource.cy,
       path: `../resources/images/${name}.png`,
   };
}
